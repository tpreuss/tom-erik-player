package dataendpoint.routes

import dataendpoint.application.DataEndpointService
import io.ktor.server.routing.*

fun Route.robotRoutes() {
    get(DataEndpointService.ROBOTS_PATH) {
        TODO()
    }
    get("${DataEndpointService.ROBOTS_PATH}/{id}") {
        TODO()
    }
}