package dataendpoint.routes

import dataendpoint.application.DataEndpointService
import io.ktor.server.routing.*

fun Route.planetRoutes() {
    get(DataEndpointService.PLANETS_PATH) {
        TODO()
    }
    get("${DataEndpointService.PLANETS_PATH}/{id}") {
        TODO()
    }
}