package dataendpoint.application

import config.AppConfig
import dataendpoint.routes.gameRoutes
import dataendpoint.routes.planetRoutes
import dataendpoint.routes.robotRoutes
import di.appModule
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.routing.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import org.koin.core.component.KoinComponent
import org.koin.ktor.plugin.Koin
import org.slf4j.LoggerFactory

class DataEndpointService : KoinComponent {

    private val logger = LoggerFactory.getLogger(javaClass)

    companion object {
        const val API_PATH = "api"
        const val GAMES_PATH = "games"
        const val PLANETS_PATH = "planets"
        const val ROBOTS_PATH = "robots"
    }

    fun start() {
        CoroutineScope(Dispatchers.IO).launch {
            logger.info("Starting DataEndpointService on port: ${AppConfig.dataEndpointPort}")
            embeddedServer(CIO, port = AppConfig.dataEndpointPort) {
                ktorConfigurations()
            }.start(wait = true)
        }

    }

    private fun Application.ktorConfigurations() {
        install(Koin) {
            modules(appModule)
        }
        install(CORS) {
            anyHost()
        }
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
            })
        }
        dataEndpoint()
    }

    private fun Application.dataEndpoint() {
        routing {
            route(API_PATH) {
                gameRoutes()
                planetRoutes()
                robotRoutes()
            }
        }
    }








}

