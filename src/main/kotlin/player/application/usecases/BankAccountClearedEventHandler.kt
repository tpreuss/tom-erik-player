package player.application.usecases

import core.eventlistener.EventHandler
import core.eventlistener.concreteevents.trading.BankAccountClearedEvent
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import player.application.PlayerApplicationService

class BankAccountClearedEventHandler(private val playerApplicationService: PlayerApplicationService) : KoinComponent,
    EventHandler<BankAccountClearedEvent> {
    private val logger = LoggerFactory.getLogger(javaClass)
    override suspend fun handle(event: BankAccountClearedEvent) {
        logger.info("Handling BankAccountCleared event for bank account: ${event.payload.playerId} balance is now ${event.payload.balance}")
        playerApplicationService.updatePlayerBankAccount(event.payload.playerId, event.payload.balance)
    }

}
