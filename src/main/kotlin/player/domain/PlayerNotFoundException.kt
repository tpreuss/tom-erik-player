package player.domain

import DungeonPlayerRuntimeException

class PlayerNotFoundException(message: String = "Player was not found when tried to be queried") : DungeonPlayerRuntimeException(message)
