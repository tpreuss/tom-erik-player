package player.domain

import core.InMemoryCrudRepository
import java.util.*

class PlayerRepositoryImpl: InMemoryCrudRepository<Player, UUID>(idExtractor = {
    player -> player.playerId
}), PlayerRepository{
    override fun findPlayerByGameId(gameId: UUID): Player? {
        return this.getAll().find { it.gameId == gameId }
    }

}