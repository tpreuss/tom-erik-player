package dev

import config.AppConfig
import core.restadapter.GameInfoResponseDto
import core.restadapter.RestAdapterException
import dev.dto.CreateGameRequestDto
import dev.dto.CreateGameResponseDto
import dev.dto.SetRoundDurationRequestDto
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import org.koin.core.component.KoinComponent
import org.slf4j.LoggerFactory
import java.util.*

class GameAdminClient(
    private val httpClient: HttpClient,
) : KoinComponent {
    private val gameServiceUrlString: String = AppConfig.gameHost

    private val logger = LoggerFactory.getLogger(javaClass)

    suspend fun createGame(maxRounds: Int, maxPlayers: Int): CreateGameResponseDto {
        val response = httpClient.post("http://${gameServiceUrlString}/games") {
            contentType(ContentType.Application.Json)
            setBody(CreateGameRequestDto(maxRounds, maxPlayers))
        }
        return when(response.status) {
            HttpStatusCode.Created -> {
                logger.info("Created game successfully")
                response.body<CreateGameResponseDto>()
            }
            else -> throw RestAdapterException("Failed to create game: ${response.status} \n ${response.bodyAsText()}")
        }
    }

    suspend fun startGame(gameId: UUID) {
        val response = httpClient.post("http://${gameServiceUrlString}/games/$gameId/gameCommands/start") {
            contentType(ContentType.Application.Json)
        }
        when (response.status) {
            HttpStatusCode.Created -> logger.info("Started game ${gameId} successfully")
            else -> throw RestAdapterException("Failed to start game: ${response.status} \n ${response.bodyAsText()}")
        }
    }

    suspend fun stopGame(gameId: UUID) {
        val response = httpClient.post("http://${gameServiceUrlString}/games/$gameId/gameCommands/end") {
            contentType(ContentType.Application.Json)
        }
        when (response.status) {
            HttpStatusCode.Created -> logger.info("Stopped game ${gameId} successfully")
            else -> {
                throw RestAdapterException("Failed to stop game $gameId: ${response.status} \n ${response.bodyAsText()} ")
            }
        }
    }


    suspend fun getAllGames(): List<GameInfoResponseDto> {
        val response = httpClient.get {
            url("http://${gameServiceUrlString}/games")
            contentType(ContentType.Application.Json)
        }
        return when(response.status) {
            HttpStatusCode.OK -> {
                logger.info("Fetched all games successfully")
                response.body<List<GameInfoResponseDto>>()
            }
            else -> throw RestAdapterException("Failed to fetch all games: ${response.status} \n ${response.bodyAsText()}")
        }

    }

    suspend fun getGame(gameId: UUID): GameInfoResponseDto? {
        val allGames = getAllGames()
        return allGames.find { it.gameId == gameId }
    }

    suspend fun setRoundDuration(gameId: UUID, duration: Int) {
        val response = httpClient.patch("http://${gameServiceUrlString}/games/$gameId/duration") {
            contentType(ContentType.Application.Json)
            setBody(SetRoundDurationRequestDto(duration))
        }
        when (response.status) {
            HttpStatusCode.OK -> logger.info("Set round duration to $duration for game $gameId")
            else -> throw RestAdapterException("Failed to set round duration to $duration for game $gameId: ${response.status} \n ${response.bodyAsText()}")
        }

    }


}