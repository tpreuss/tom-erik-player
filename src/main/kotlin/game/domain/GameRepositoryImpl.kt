package game.domain

import core.InMemoryCrudRepository
import java.util.*

class GameRepositoryImpl : InMemoryCrudRepository<Game, UUID>(idExtractor = {
    game -> game.gameId
}), GameRepository {
    override fun findAllByGameStatusBetween(gameStatus1: GameStatus, gameStatus2: GameStatus): List<Game> {
        return this.findAll {
            it.gameStatus.ordinal in gameStatus1.ordinal..gameStatus2.ordinal
        }
    }
}