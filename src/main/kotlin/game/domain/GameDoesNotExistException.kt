package game.domain

import DungeonPlayerRuntimeException
import java.util.*

class GameDoesNotExistException(
    message: String = "Game was not found",
    val gameId: UUID
) : DungeonPlayerRuntimeException(message) {

    constructor(gameId: UUID) : this("Game with id $gameId was not found", gameId)

}
