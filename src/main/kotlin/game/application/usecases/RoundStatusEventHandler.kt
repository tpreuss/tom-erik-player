package game.application.usecases

import core.eventlistener.EventHandler
import core.eventlistener.concreteevents.game.RoundStatusEvent
import game.application.GameApplicationService
import org.slf4j.LoggerFactory

class RoundStatusEventHandler(private val gameApplicationService: GameApplicationService) : EventHandler<RoundStatusEvent> {
    private val logger = LoggerFactory.getLogger(javaClass)

    override suspend fun handle(event: RoundStatusEvent) {
        logger.info("Handling RoundStatus event for round: ${event.payload.roundId}")
        gameApplicationService.roundStarted(event.payload.roundNumber)
    }

}