package core.domainprimitives.purchasing

import core.domainprimitives.DomainPrimitiveException
import kotlinx.serialization.Serializable

@Serializable
data class Capability protected constructor(
    private val type : CapabilityType,
    private val level : Int
) {
    companion object {
        const val MIN_LEVEL = 0;
        const val MAX_LEVEL = 5;

        /**
         * @param type
         * @return Base capability for the given type
         */
        fun baseForType(type: CapabilityType?): Capability {
            return forTypeAndLevel(type, MIN_LEVEL)
        }

        /**
         * @param type
         * @param level
         * @return Capability for given type and level
         */
        fun forTypeAndLevel(
            type: CapabilityType?,
            level: Int?
        ): Capability {
            if (type == null || level == null) throw DomainPrimitiveException("type == null || level == null")
            if (level < MIN_LEVEL || level > MAX_LEVEL) throw DomainPrimitiveException(
                "level < MIN_LEVEL || level > MAX_LEVEL"
            )
            val capability = Capability(type, level)
            return capability
        }


        /**
         * @return Complete list of all base capabities a robot can have
         */
        fun allBaseCapabilities(): List<Capability> {
            val allBaseCapabilities: MutableList<Capability> =
                ArrayList<Capability>()
            for (capabilityType in CapabilityType.entries) {
                allBaseCapabilities.add(
                    baseForType(
                        capabilityType
                    )
                )
            }
            return allBaseCapabilities
        }
    }

    fun nextLevel(): Capability {
        return if (level < Capability.MAX_LEVEL) Capability.forTypeAndLevel(
            type, level + 1
        ) else throw DomainPrimitiveException("Already at max level")
    }


    fun isMinimumLevel(): Boolean {
        return level == Capability.MIN_LEVEL
    }


    fun isMaximumLevel(): Boolean {
        return level == Capability.MAX_LEVEL
    }


    override fun toString(): String {
        return "$type-$level"
    }

    fun toStringForCommand(): String {
        return type.name + "_" + level
    }
}
