package core.domainprimitives.command

import kotlinx.serialization.Serializable

@Serializable
enum class CommandType(private val stringValue: String) {
    MOVEMENT("movement"),
    BATTLE("battle"),
    MINING("mining"),
    REGENERATE("regenerate"),
    BUYING("buying"),
    SELLING("selling");

    override fun toString(): String {
        return this.stringValue
    }

}
