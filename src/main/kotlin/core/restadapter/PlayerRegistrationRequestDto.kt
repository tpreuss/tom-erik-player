package core.restadapter

import kotlinx.serialization.Serializable
@Serializable
data class PlayerRegistrationRequestDto(val name: String, val email: String)