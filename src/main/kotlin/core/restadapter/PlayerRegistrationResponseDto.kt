@file:UseSerializers(UUIDSerializer::class)
package core.restadapter

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class PlayerRegistrationResponseDto(
    val playerId : UUID,
    val name : String,
    val email : String,
    val playerExchange : String,
    val playerQueue : String,
)
