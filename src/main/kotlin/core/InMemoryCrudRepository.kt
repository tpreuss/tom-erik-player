package core

import java.util.concurrent.ConcurrentHashMap

abstract class InMemoryCrudRepository<T, ID>(private val idExtractor : (T) -> ID) : CrudRepository<T, ID> {

    private val entityMap = ConcurrentHashMap<ID, T>()

    override fun <S : T> save(entity: S): S {
        val entityId = getId(entity)
        entityMap[entityId] = entity
        return entity
    }

    override fun <S : T> saveAll(entities: Iterable<S>): Iterable<S> {
        val savedEntities = mutableListOf<S>()
        for (entity in entities) {
            val savedEntity = save(entity)
            savedEntities.add(savedEntity)
        }
        return savedEntities
    }

    override fun findById(id: ID): T? {
        return entityMap[id]
    }

    override fun existsById(id: ID): Boolean {
        return entityMap.containsKey(id)
    }

    override fun getAll(): List<T> {
        return entityMap.values.toList()
    }

    override fun findAll(predicate : (T) -> Boolean): List<T> {
        return getAll().filter(predicate)
    }

    override fun findAllById(ids: Iterable<ID>): List<T> {
        return ids.mapNotNull { id -> entityMap[id] }
    }

    override fun count(): Long {
        return entityMap.size.toLong()
    }

    override fun deleteById(id: ID) {
        entityMap.remove(id)
    }

    override fun delete(entity: T) {
        val entityId = getId(entity)
        entityMap.remove(entityId)
    }

    override fun deleteAllById(ids: Iterable<ID>) {
        for (id in ids) {
            entityMap.remove(id)
        }
    }

    override fun deleteAll(entities: Iterable<T>) {
        for (entity in entities) {
            val entityId = getId(entity)
            entityMap.remove(entityId)
        }
    }

    override fun deleteAll() {
        entityMap.clear()
    }

    private fun getId(entity: T): ID {
        return idExtractor(entity)
    }

}
