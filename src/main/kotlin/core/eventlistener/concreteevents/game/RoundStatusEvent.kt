package core.eventlistener.concreteevents.game

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.*

@Serializable
@SerialName(EventType.ROUND_STATUS)
data class RoundStatusEvent(
    override val header: GameEventHeader,
    val payload: RoundStatusPayload
) : GameEvent()
@Serializable
data class RoundStatusPayload(
    @Serializable(with = UUIDSerializer::class)
    val gameId: UUID,
    @Serializable(with = UUIDSerializer::class)
    val roundId: UUID,
    val roundNumber: Int,
    val roundStatus: RoundStatusType,
    val impreciseTimings: ImpreciseTimings,
    val impreciseTimingPredictions: ImpreciseTimingPredictions,
)



