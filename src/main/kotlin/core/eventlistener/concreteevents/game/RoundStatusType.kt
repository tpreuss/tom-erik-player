package core.eventlistener.concreteevents.game

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class RoundStatusType {
    @SerialName("started")
    STARTED,
    @SerialName("command input ended")
    COMMAND_INPUT_ENDED,
    @SerialName("ended")
    ENDED
}