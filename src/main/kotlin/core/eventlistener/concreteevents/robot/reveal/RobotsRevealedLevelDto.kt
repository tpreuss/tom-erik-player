package core.eventlistener.concreteevents.robot.reveal

import kotlinx.serialization.Serializable

@Serializable
data class RobotsRevealedLevelDto(
    private val healthLevel: Int,
    private val energyLevel: Int,
    private val energyRegenLevel : Int,
    private val damageLevel : Int,
    private val miningSpeedLevel : Int,
    private val miningLevel : Int,
    private val storageLevel : Int,
) {
    companion object {
        private const val DEFAULT_LEVEL = 2;
        /**
         * Factory method with default values for testing purposes
         */
        fun defaults() : RobotsRevealedLevelDto{
            return RobotsRevealedLevelDto(
                DEFAULT_LEVEL,
                DEFAULT_LEVEL,
                DEFAULT_LEVEL,
                DEFAULT_LEVEL,
                DEFAULT_LEVEL,
                DEFAULT_LEVEL,
                DEFAULT_LEVEL,
            )
        }
    }

}
