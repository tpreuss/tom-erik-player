package core.restadapter

import config.AppConfig
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import org.apache.hc.client5.http.HttpHostConnectException
import org.junit.jupiter.api.Test
import player.domain.PlayerNotFoundException
import player.domain.PlayerRegistrationException
import util.Ktor.Companion.applyCommonConfiguration
import java.util.*
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse

class GameServiceRESTAdapterConnectionFailureIntegrationTest {

    private val appConfig = AppConfig
    private lateinit var gameServiceRESTAdapter: GameServiceRESTAdapter

    @Test
    fun `test fetchAllGames throws RESTAdapterException if HttpStatus not 200`() {
        val mockEngine = MockEngine { request ->
            println("Request: ${request}")
            when (request.url.fullPath) {
                "/games" -> respond("", status = HttpStatusCode.NotFound)
                else -> error("Unhandled ${request.url.fullPath}")
            }
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<RestAdapterException> {
            runBlocking {
                gameServiceRESTAdapter.fetchAllGames()
            }
        }
    }

    @Test
    fun `RestAdapterException thrown when otherwise HttpHostConnectException is thrown`() {
        val mockEngine = MockEngine { request ->
            println("Request: ${request}")
            when (request.url.fullPath) {
                "/games" -> throw HttpHostConnectException("Service not available")
                else -> error("Unhandled ${request.url.fullPath}")
            }
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<RestAdapterException> {
            runBlocking {
                gameServiceRESTAdapter.fetchAllGames()
            }
        }
    }

    @Test
    fun `sendGetRequestForPlayerDto throws PlayerNotFoundException when Player is not registered`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.NotFound)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<PlayerNotFoundException> {
            runBlocking {
                gameServiceRESTAdapter.sendGetRequestForPlayerDto()
            }
        }
    }

    @Test
    fun `sendGetRequestForPlayerDto throws PlayerRegistrationException when unexpected HttpStatus is returned`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.InternalServerError)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<PlayerRegistrationException> {
            runBlocking {
                gameServiceRESTAdapter.sendGetRequestForPlayerDto()
            }
        }
    }

    @Test
    fun `joinGame returns true when HttpStatus is HttpStatus OK`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.OK)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        runBlocking {
            assert(
                gameServiceRESTAdapter.joinGame(
                    gameId = UUID.randomUUID(),
                    playerId = UUID.randomUUID()
                )
            )
        }

    }

    @Test
    fun `joinGame returns false when Player or Game is not found`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.NotFound)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFalse {
            runBlocking {
                gameServiceRESTAdapter.joinGame(
                    gameId = UUID.randomUUID(),
                    playerId = UUID.randomUUID()
                )
            }
        }
    }

    @Test
    fun `joinGame returns false when Game is full or has been started`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.BadRequest)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFalse {
            runBlocking {
                gameServiceRESTAdapter.joinGame(
                    gameId = UUID.randomUUID(),
                    playerId = UUID.randomUUID()
                )
            }
        }
    }

    @Test
    fun `joinGame returns false when unexpected HttpStatus is returned`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.InternalServerError)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFalse {
            runBlocking {
                gameServiceRESTAdapter.joinGame(
                    gameId = UUID.randomUUID(),
                    playerId = UUID.randomUUID()
                )
            }
        }
    }

    @Test
    fun `joinGame throws RestAdapterException when HttpHostConnectException is thrown`() {
        val mockEngine = MockEngine { request ->
            throw HttpHostConnectException("Service not available")
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<RestAdapterException> {
            runBlocking {
                gameServiceRESTAdapter.joinGame(
                    gameId = UUID.randomUUID(),
                    playerId = UUID.randomUUID()
                )
            }
        }
    }

    @Test
    fun `register player throws PlayerRegistrationException when HttpStatus is not HttpStatus Created`() {
        val mockEngine = MockEngine { request ->
            respond("", status = HttpStatusCode.InternalServerError)
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<PlayerRegistrationException> {
            runBlocking {
                gameServiceRESTAdapter.registerPlayer("","")
            }
        }
    }

    @Test
    fun `register player throws RestAdapterException when HttpHostConnectException is thrown`() {
        val mockEngine = MockEngine { request ->
            throw HttpHostConnectException("Service not available")
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        assertFailsWith<RestAdapterException> {
            runBlocking {
                gameServiceRESTAdapter.registerPlayer("","")
            }
        }
    }

    @Test
    fun `registerPlayer returns PlayerRegistrationResponseDto when HttpStatus is HttpStatus Created`() {
        val mockEngine = MockEngine { request ->
            respond(
                content = """
                    {
                        "playerId": "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
                        "name": "TestPlayer",
                        "email": "Test@testmail.com",
                        "playerExchange": "player-TestPlayer",
                        "playerQueue": "player-TestPlayer"
                    }
                    """.trimIndent(),
                status = HttpStatusCode.Created,
                headers = headersOf("Content-Type", "application/json")
            )
        }
        gameServiceRESTAdapter = GameServiceRESTAdapter(
            appConfig = appConfig,
            httpClient = createHttpClient(mockEngine)
        )
        runBlocking {
            val playerRegistrationResponseDto = gameServiceRESTAdapter.registerPlayer("Testplayer", "Test@testmail.com")
            assert(playerRegistrationResponseDto.playerId == UUID.fromString("a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11"))
            assert(playerRegistrationResponseDto.name == "TestPlayer")
            assert(playerRegistrationResponseDto.email == "Test@testmail.com")
            assert(playerRegistrationResponseDto.playerExchange == "player-TestPlayer")
            assert(playerRegistrationResponseDto.playerQueue == "player-TestPlayer")
        }
    }


    private fun createHttpClient(mockEngine: MockEngine): HttpClient {
        return HttpClient(mockEngine) {
            applyCommonConfiguration()
        }
    }


}